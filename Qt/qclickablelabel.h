#ifndef QCLICKABLELABEL_H
#define QCLICKABLELABEL_H

#include <QLabel>
#include <QMouseEvent>
#include <QEvent>

class QClickableLabel : public QLabel
{
    Q_OBJECT
public:
    explicit QClickableLabel(QWidget *parent = nullptr);
    explicit QClickableLabel(QString path);
    ~QClickableLabel();
    void mouseDoubleClickEvent(QMouseEvent *ev);

    QString path;

signals:
    void mouseDoubleClick();

};

#endif // QCLICKABLELABEL_H
