#ifndef TBROWSER_H
#define TBROWSER_H

#include <QMainWindow>
#include <QLayoutItem>
#include <QGridLayout>
#include <QLabel>
#include "qclickablelabel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class TBrowser; }
QT_END_NAMESPACE

class TBrowser : public QMainWindow
{
    Q_OBJECT

public:
    TBrowser(QWidget *parent = nullptr);
    ~TBrowser();

private slots:
    void on_checkBox_airplane_clicked();
    void on_checkBox_lotus_clicked();
    void on_checkBox_panda_clicked();
    void on_checkBox_pyramid_clicked();
    void on_checkBox_strawberry_clicked();
    void on_checkBox_all_clicked();
    void labelDoubleClick();

private:
    Ui::TBrowser *ui;
    int row;
    int column;
    int widthColumns;

    void fetchImages(int columns);
    QVBoxLayout * createImage(const QString filename, const QString folderName, const QString path);
    QStringList checkBoxClicked();
    void removeItem(QLayoutItem *item);
    void removeImages();
    void removeGridLayout(QGridLayout *gridLayout);
    QGridLayout *randomizeGridLayout(QGridLayout *gridLayout, int columns);
    void resizeEvent(QResizeEvent* event);
};

#endif // TBROWSER_H
