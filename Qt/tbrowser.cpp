#include "tbrowser.h"
#include "ui_tbrowser.h"
#include "qclickablelabel.h"
#include "picdialog.h"
#include <QPixmap>
#include <QDir>
#include <QVBoxLayout>
#include <QLabel>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <random>
#include <QFileDialog>

TBrowser::TBrowser(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::TBrowser)
{
    ui->setupUi(this);
    this->setWindowTitle("Thumbnail Browser");
    widthColumns = 3;
}

TBrowser::~TBrowser()
{
    removeImages();
    delete ui;
}

// Gestione delle immagini in base alla larghezza della finestra
void TBrowser::resizeEvent(QResizeEvent* event)
{
   QMainWindow::resizeEvent(event);
   QSize size = this->size();
   bool set = false;
   int width = size.width();

   if(width > 1015 && widthColumns != 6) {
       widthColumns = 6;
       set = true;
   }

   if(width < 1015 && width > 867 && widthColumns != 5) {
       widthColumns = 5;
       set = true;
   }

   if(width < 867 && width > 720 && widthColumns != 4) {
       widthColumns = 4;
       set = true;
   }

   if(width < 720 && width > 576 && widthColumns != 3) {
       widthColumns = 3;
       set = true;
   }

   if(width < 576 && width > 419 && widthColumns != 2) {
       widthColumns = 2;
       set = true;
   }

   if(width < 419 && widthColumns != 1) {
       widthColumns = 1;
       set = true;
   }

   if(set) {
       removeImages();
       fetchImages(widthColumns);
   }
}

// Carica le immagini e le inserisce in un gridLayout indicando su
// quante colonne disporle
void TBrowser::fetchImages(int columns)
{
    row = 0;
    column = 0;

    QStringList foldersName = checkBoxClicked();
    QGridLayout *gridLayout = new QGridLayout();

    foreach(QString folderName, foldersName) {
        QList paths = QCoreApplication::applicationDirPath().split("Qt");
        QString path = paths[0] + "Qt/dataset/" + folderName;
        QDir directory(path);
        QStringList images = directory.entryList(QStringList() << "*.jpg" << "*.JPG",QDir::Files);

        foreach(QString filename, images) {

            QVBoxLayout *VStack = createImage(filename, folderName, path);

            if(column%columns == 0) {
                row++;
                column = 0;
            }

            gridLayout->addItem(VStack, row, column);

            column++;
        }
    }

    QGridLayout *gridLayoutRandom = randomizeGridLayout(gridLayout, columns);

    gridLayoutRandom->setAlignment(Qt::AlignCenter);
    gridLayoutRandom->setSpacing(30);
    ui->scrollContents->setLayout(gridLayoutRandom);
}

// Crea una nuova griglia randomizzando gli elementi della grigli passata
QGridLayout *TBrowser::randomizeGridLayout(QGridLayout *gridLayout, int columns)
{
    QGridLayout *gridLayoutRandom = new QGridLayout();
    QLayoutItem* item;
    int number, index;
    column = 0;
    row = 0;

    while ((number = gridLayout->count()) > 0) {
        index = rand() % number;
        item = gridLayout->takeAt(index);

        if(column%columns == 0) {
            row++;
            column = 0;
        }

        gridLayoutRandom->addItem(item, row, column);

        column++;
    }

    removeGridLayout(gridLayout);
    return gridLayoutRandom;
}

// Crea un verticalLayout con immagine e label al suo interno
QVBoxLayout *TBrowser::createImage(const QString filename, const QString folderName, const QString path)
{
    QVBoxLayout *VStack = new QVBoxLayout();
    QPixmap pix(path + "/" + filename);
    QClickableLabel *picture = new QClickableLabel(path + "/" + filename);
    QLabel *label = new QLabel(folderName);

    picture->setFixedSize(120, 120);
    picture->setPixmap(pix.scaled(120, 120, Qt::KeepAspectRatio));
    connect(picture, SIGNAL(mouseDoubleClick()), this, SLOT(labelDoubleClick()));

    label->setAlignment(Qt::AlignCenter);

    VStack->addWidget(picture);
    VStack->addWidget(label);

    return VStack;
}

// Ritorna la lista di checkBox selezionate
QStringList TBrowser::checkBoxClicked()
{
    QStringList clickedList = {};

    if(ui->checkBox_airplane->isChecked()) {
        clickedList.append("airplane");
    }

    if(ui->checkBox_panda->isChecked()) {
        clickedList.append("panda");
    }

    if(ui->checkBox_lotus->isChecked()) {
        clickedList.append("lotus");
    }

    if(ui->checkBox_pyramid->isChecked()) {
        clickedList.append("pyramid");
    }

    if(ui->checkBox_strawberry->isChecked()) {
        clickedList.append("strawberry");
    }

    return clickedList;
}

// Rimuove un verticalLayout e i widget contenuti
void TBrowser::removeItem(QLayoutItem *item)
{
    QVBoxLayout *verticalLayout = qobject_cast<QVBoxLayout*>(item->layout());

    while ((item = verticalLayout->takeAt(0)) != NULL) {
        delete item->widget();
        delete item;
    }
    delete verticalLayout;
}

// Rimuove tutte le immagini e il gridLayout
void TBrowser::removeImages()
{
    QGridLayout *gridLayout = qobject_cast<QGridLayout*>(ui->scrollContents->layout());

    if (gridLayout != NULL) {
        QLayoutItem* item;

        while ((item = gridLayout->takeAt(0)) != NULL) {
            removeItem(item);
        }
        delete gridLayout;
    }

    row = 0;
    column = 0;
}

// Rimuove tutte le immagini e il gridLayout da un grid passato
void TBrowser::removeGridLayout(QGridLayout *gridLayout)
{
    if (gridLayout != NULL) {
        QLayoutItem* item;

        while ((item = gridLayout->takeAt(0)) != NULL) {
            removeItem(item);
        }
        delete gridLayout;
    }

    row = 0;
    column = 0;
}

// Private slots

void TBrowser::on_checkBox_airplane_clicked()
{
    if(!ui->checkBox_airplane->isChecked()) {
        ui->checkBox_all->setChecked(false);
    }
    removeImages();
    fetchImages(widthColumns);
}


void TBrowser::on_checkBox_lotus_clicked()
{
    if(!ui->checkBox_lotus->isChecked()) {
        ui->checkBox_all->setChecked(false);
    }
    removeImages();
    fetchImages(widthColumns);
}


void TBrowser::on_checkBox_panda_clicked()
{
    if(!ui->checkBox_panda->isChecked()) {
        ui->checkBox_all->setChecked(false);
    }
    removeImages();
    fetchImages(widthColumns);
}


void TBrowser::on_checkBox_pyramid_clicked()
{
    if(!ui->checkBox_pyramid->isChecked()) {
        ui->checkBox_all->setChecked(false);
    }
    removeImages();
    fetchImages(widthColumns);
}


void TBrowser::on_checkBox_strawberry_clicked()
{
    if(!ui->checkBox_strawberry->isChecked()) {
        ui->checkBox_all->setChecked(false);
    }
    removeImages();
    fetchImages(widthColumns);
}


void TBrowser::on_checkBox_all_clicked()
{
    if(ui->checkBox_all->isChecked()) {
        ui->checkBox_airplane->setChecked(true);
        ui->checkBox_strawberry->setChecked(true);
        ui->checkBox_lotus->setChecked(true);
        ui->checkBox_panda->setChecked(true);
        ui->checkBox_pyramid->setChecked(true);

        removeImages();
        fetchImages(widthColumns);
    }
}

// Gestione dell'evento double click e apertura di una nuova Dialog
void TBrowser::labelDoubleClick()
{
    QClickableLabel* picture = qobject_cast<QClickableLabel*>(QObject::sender());

    PicDialog detailView(picture);
    detailView.setModal(true);
    detailView.exec();
}

