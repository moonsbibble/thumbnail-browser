#include "picdialog.h"
#include "ui_picdialog.h"
#include "qclickablelabel.h"
#include <QVBoxLayout>
#include <QDebug>

PicDialog::PicDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PicDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Detail View");
}

PicDialog::PicDialog(QClickableLabel *picture) :
    ui(new Ui::PicDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Detail View");

    QPixmap pix(picture->path);
    ui->picture->setFixedSize(pix.size());
    ui->picture->setPixmap(pix);
}

PicDialog::~PicDialog()
{
    delete ui;
}
