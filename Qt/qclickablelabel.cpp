#include "qclickablelabel.h"

QClickableLabel::QClickableLabel(QWidget *parent) : QLabel(parent)
{
    path = "";
}

QClickableLabel::QClickableLabel(QString path) : QLabel("")
{
    this->path = path;
}

QClickableLabel::~QClickableLabel()
{
    path = "";
}

void QClickableLabel::mouseDoubleClickEvent(QMouseEvent *ev)
{
    emit mouseDoubleClick();
}
