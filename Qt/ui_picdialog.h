/********************************************************************************
** Form generated from reading UI file 'picdialog.ui'
**
** Created by: Qt User Interface Compiler version 6.2.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PICDIALOG_H
#define UI_PICDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_PicDialog
{
public:
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer_sx;
    QSpacerItem *verticalSpacer_up;
    QSpacerItem *horizontalSpacer_dx;
    QLabel *picture;
    QSpacerItem *verticalSpacer_down;

    void setupUi(QDialog *PicDialog)
    {
        if (PicDialog->objectName().isEmpty())
            PicDialog->setObjectName(QString::fromUtf8("PicDialog"));
        PicDialog->resize(600, 600);
        PicDialog->setMinimumSize(QSize(600, 600));
        PicDialog->setMaximumSize(QSize(600, 600));
        gridLayout = new QGridLayout(PicDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalSpacer_sx = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_sx, 1, 0, 1, 1);

        verticalSpacer_up = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_up, 0, 1, 1, 1);

        horizontalSpacer_dx = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_dx, 1, 2, 1, 1);

        picture = new QLabel(PicDialog);
        picture->setObjectName(QString::fromUtf8("picture"));
        picture->setMinimumSize(QSize(0, 120));
        picture->setMaximumSize(QSize(120, 120));

        gridLayout->addWidget(picture, 1, 1, 1, 1);

        verticalSpacer_down = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_down, 2, 1, 1, 1);


        retranslateUi(PicDialog);

        QMetaObject::connectSlotsByName(PicDialog);
    } // setupUi

    void retranslateUi(QDialog *PicDialog)
    {
        PicDialog->setWindowTitle(QCoreApplication::translate("PicDialog", "Dialog", nullptr));
        picture->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class PicDialog: public Ui_PicDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PICDIALOG_H
