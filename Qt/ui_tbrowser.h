/********************************************************************************
** Form generated from reading UI file 'tbrowser.ui'
**
** Created by: Qt User Interface Compiler version 6.2.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TBROWSER_H
#define UI_TBROWSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TBrowser
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollContents;
    QVBoxLayout *checkBoxLayout;
    QSpacerItem *verticalSpacer_top;
    QCheckBox *checkBox_all;
    QCheckBox *checkBox_airplane;
    QCheckBox *checkBox_lotus;
    QCheckBox *checkBox_panda;
    QCheckBox *checkBox_pyramid;
    QCheckBox *checkBox_strawberry;
    QSpacerItem *verticalSpacer_bottom;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *TBrowser)
    {
        if (TBrowser->objectName().isEmpty())
            TBrowser->setObjectName(QString::fromUtf8("TBrowser"));
        TBrowser->resize(690, 600);
        TBrowser->setMinimumSize(QSize(296, 0));
        TBrowser->setMaximumSize(QSize(1080, 16777215));
        centralwidget = new QWidget(TBrowser);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        scrollArea = new QScrollArea(centralwidget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollContents = new QWidget();
        scrollContents->setObjectName(QString::fromUtf8("scrollContents"));
        scrollContents->setGeometry(QRect(0, 0, 562, 525));
        scrollContents->setMinimumSize(QSize(0, 0));
        scrollArea->setWidget(scrollContents);

        horizontalLayout->addWidget(scrollArea);

        checkBoxLayout = new QVBoxLayout();
        checkBoxLayout->setSpacing(35);
        checkBoxLayout->setObjectName(QString::fromUtf8("checkBoxLayout"));
        verticalSpacer_top = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        checkBoxLayout->addItem(verticalSpacer_top);

        checkBox_all = new QCheckBox(centralwidget);
        checkBox_all->setObjectName(QString::fromUtf8("checkBox_all"));

        checkBoxLayout->addWidget(checkBox_all);

        checkBox_airplane = new QCheckBox(centralwidget);
        checkBox_airplane->setObjectName(QString::fromUtf8("checkBox_airplane"));

        checkBoxLayout->addWidget(checkBox_airplane);

        checkBox_lotus = new QCheckBox(centralwidget);
        checkBox_lotus->setObjectName(QString::fromUtf8("checkBox_lotus"));

        checkBoxLayout->addWidget(checkBox_lotus);

        checkBox_panda = new QCheckBox(centralwidget);
        checkBox_panda->setObjectName(QString::fromUtf8("checkBox_panda"));

        checkBoxLayout->addWidget(checkBox_panda);

        checkBox_pyramid = new QCheckBox(centralwidget);
        checkBox_pyramid->setObjectName(QString::fromUtf8("checkBox_pyramid"));

        checkBoxLayout->addWidget(checkBox_pyramid);

        checkBox_strawberry = new QCheckBox(centralwidget);
        checkBox_strawberry->setObjectName(QString::fromUtf8("checkBox_strawberry"));

        checkBoxLayout->addWidget(checkBox_strawberry);

        verticalSpacer_bottom = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        checkBoxLayout->addItem(verticalSpacer_bottom);


        horizontalLayout->addLayout(checkBoxLayout);

        TBrowser->setCentralWidget(centralwidget);
        menubar = new QMenuBar(TBrowser);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 690, 22));
        TBrowser->setMenuBar(menubar);
        statusbar = new QStatusBar(TBrowser);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        TBrowser->setStatusBar(statusbar);

        retranslateUi(TBrowser);

        QMetaObject::connectSlotsByName(TBrowser);
    } // setupUi

    void retranslateUi(QMainWindow *TBrowser)
    {
        TBrowser->setWindowTitle(QCoreApplication::translate("TBrowser", "TBrowser", nullptr));
        checkBox_all->setText(QCoreApplication::translate("TBrowser", "All", nullptr));
        checkBox_airplane->setText(QCoreApplication::translate("TBrowser", "Airplane", nullptr));
        checkBox_lotus->setText(QCoreApplication::translate("TBrowser", "Lotus", nullptr));
        checkBox_panda->setText(QCoreApplication::translate("TBrowser", "Panda", nullptr));
        checkBox_pyramid->setText(QCoreApplication::translate("TBrowser", "Pyramid", nullptr));
        checkBox_strawberry->setText(QCoreApplication::translate("TBrowser", "Strawberry", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TBrowser: public Ui_TBrowser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TBROWSER_H
