#ifndef PICDIALOG_H
#define PICDIALOG_H

#include <QDialog>
#include <QLabel>
#include "qclickablelabel.h"

namespace Ui {
class PicDialog;
}

class PicDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PicDialog(QWidget *parent = nullptr);
    explicit PicDialog(QClickableLabel *picture);
    ~PicDialog();

private:
    Ui::PicDialog *ui;
};

#endif // PICDIALOG_H
