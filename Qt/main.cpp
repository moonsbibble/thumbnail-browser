#include "tbrowser.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TBrowser w;
    w.show();
    return a.exec();
}
