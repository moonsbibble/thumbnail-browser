# Thumbnail Browser

## Description
This project features an interface written in C++ using Qt. The interface displays a folder of images arranged in a grid and includes a series of checkboxes to select the types of images to be displayed. Additionally, double-clicking an image will display it at full size. The interface also includes responsive adaptation of images based on the size of the window.

![Browser](images/view.png)

## Implementation
For the Qt project, I decided to use *QLabel* objects (specifically a custom *QClickableLabel* type) to insert photos and display them. The random display is there all the time, not only when the checkBox all is clicked, but also with all other combinations of categories selected.

For each photo, I chose to leave the label "category" even when the category to be displayed is only one, also to make sure to handle the doubleClick event I created a new class *QClickableLabel* that keeps the path of the original photo and handles this mouse event.

The main window has a maximum and minimum size, and the content in the scrollArea is handled to show the maximum possible amount of images in width.

## Class
#### QClickableLabel
The *QClickableLabel* class inherits from the *QLabel* class and allows the mouseDoubleClickEvent to be handled. The class has a public *path* attribute that maintains the path considering that the label is an image.
```c++
class QClickableLabel : public QLabel
QString path
```

##### Default constructor
Constructor with pointer to a *QWidget* (null by default).
```c++
explicit QClickableLabel(QWidget *parent = nullptr)
```

##### Path constructor
Constructor that saves as an internal attribute a path given as a parameter.
```c++
explicit QClickableLabel(QString path)
```

##### Default destructor
Automatic destructor.
```c++
~QClickableLabel()
```

##### Mouse DoubleClick
Method that receives a mouse event as input.
```c++
void mouseDoubleClickEvent(QMouseEvent *ev)
```

##### Public slots
Slot to detect double mouse click.
```c++
void mouseDoubleClick()
```

## Class
#### PicDialog
The *PicDialog* class is used to show a detailed view when we double-click on a *QClickableLabel*.
```c++
class PicDialog : public QDialog
```

##### Default constructor
Constructor with pointer to a *QWidget* (null by default).
```c++
explicit PicDialog(QWidget *parent = nullptr)
```

##### Picture constructor
Constructor that uses a pointer to a *QClickableLabel* to access its path attribute and construct a full-size *QPixmap*.
```c++
explicit PicDialog(QClickableLabel *picture)
```

##### Default destructor
Automatic destructor.
```c++
~PicDialog()
```

## Class
#### TBrowser
The *TBrowser* class is the class responsible for the main view. As public attributes we have the number of rows and columns on which to arrange the photo thumbnails, as well as an attrbitute *widthColumns* that indicates the total width to handle the responsive design of the interface.
```c++
class TBrowser : public QMainWindow
int row
int column
int widthColumns
```

##### Default constructor
Constructor with pointer to a *QWidget*.
```c++
TBrowser(QWidget *parent = nullptr)
```

##### Default destructor
Automatic destructor.
```c++
~TBrowser()
```

##### Resize event
Method that handles responsive design by taking as input an interface resize event and setting the number of columns based on a predetermined range of window widths. Choosing the number of columns reloads the images with this arrangement.
```c++
void resizeEvent(QResizeEvent* event)
```

##### Grid loading
The columns set by the method above are passed to the image fetch method. This method is responsible for creating a *QGridLayout* grid and for each folder and for each image inside enter a *QVBoxLayout* into a cell by creating it in a special function. Once all the grid cells are filled, the randomization method is called to randomly arrange the images as required by the delivery.
```c++
void fetchImages(int columns)
```

##### Cell creation
This method creates a *QVBoxLayout* passing as input the file name, the path to the folder and the folder name. It creates the image, creates the *QClickableLabel* saving the path and a *QLabel* with the folder name. The size of the image is set and inserted into the *QClickableLabel*, finally the mouse double click event is connected and the two components inserted into the *QVBoxLayout*.
```c++
QVBoxLayout * createImage(const QString filename, const QString folderName, const QString path)
```

##### Image randomization
The randomization is necessary because of the delivery requirement of not arranging the images precisely, but randomly. In this case I pass a *QGridLayout* and its number of columns and for each cell I calculate a randomized index and rearrange all the images. At the end of the method the old (empty) grid is removed by a special method and the new randomized grid is passed to the output.
```c++
QGridLayout *randomizeGridLayout(QGridLayout *gridLayout, int columns)
```

##### Selected checkboxes
It performs a check to figure out which checkbox list is selected and thus the type of images to display.
```c++
QStringList checkBoxClicked()
```

##### Grid removal
All elements of the grid passed in are removed with the *removeItem* function and the grid deleted.
```c++
void removeGridLayout(QGridLayout *gridLayout)
```

##### Image removal
Casts the layout present in the ui to *QGridLayout* and performs the same function as *removeGridLayout*.
```c++
void removeImages()
```

##### Item removal
Cast the item layout to *QVBoxLayout*, then delete all items inside and delete the item.
```c++
void removeItem(QLayoutItem *item)
```

##### DoubleClick event
Cast the sender to *QClickableLabel* so you can build a PicDialog with the dedicated constructor and execute the detailed view.
```c++
void labelDoubleClick()
```

##### Private slots
Each new checkbox selected invokes the function of calculating selections and fetching images.
```c++
void on_checkBox_airplane_clicked()
void on_checkBox_lotus_clicked()
void on_checkBox_panda_clicked()
void on_checkBox_pyramid_clicked()
void on_checkBox_strawberry_clicked()
void on_checkBox_all_clicked()
void labelDoubleClick()
```

## Video
The video below shows the browser in action and you can see all the features described above.

![Thumbnail Browser](images/video.mov){width=100%}
